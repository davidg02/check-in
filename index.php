<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Check-In - Vuelo AA001</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0.900;1,100;1,300;1,400;1,
    500;1,700;1,900&display=swap" rel="stylsheet">
    <link rel="stylesheet" href="assets/css/estilos.css">
</head>

<body>
    <main>
        <div class="contenedor__todo">
            <h3>Bienvenidos al vuelo AA001</h3>
            <p>Por favor ingrese todos los datos correspondiente</p>
            <form action="php/alta.php" method="POST" class="formulario__check-in">
                <input type="text" placeholder="Nombre Completo" name="nombre">
                <input type="text" placeholder="Codigo de reserva. Ej:QWDCBN" name="reserva">
                <input type="text" placeholder="Numero de pasaporte" name="pasaporte">
                <input type="text" placeholder="Contacto de emergencia (Telefono)" name="telefono">
                <input type="text" placeholder="Correo electronico" name= "correo">
                <button>Finalizar Check-In</button>
            </form>
        </div>
    </main>
</body>
</html>